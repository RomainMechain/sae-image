# README SAE image 

## Introduction :


## Questions :

### Question numéro A.1 :

Lorsque l'on lance la commande fournie `display logo.bmp`, il n'y a, à première vue, aucune erreur car l'image s'affiche correctement, cependant lorsque l'on regarde sur le terminal, on peut voir que l'erreur  `display-im6.q16: length and filesize do not match 'logotest.bmp' @ error/bmp.c/ReadBMPImage/854.` qui indique que la taille indiqué dans le fichier n'est pas la vrai taille. Il suffit donc pour cela de changer les octets correspondant à la taille afin de les faire correspondre. On change donc le troisième octet en passant de `99` à `9A`. 
`